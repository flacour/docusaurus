import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Actualités Projet Eléa

:::danger URGENT 23/06/2023
- [ ] Attente du tuto de création des groupes de Bertrand
- [ ] 
:::

## 0 - ToDo Liste/questions en vrac
##### Déploiement 
- [ ] 
- [ ] comment remonter les problèmes aux pilotes/National ? (exemple : l'aide très très lente)
= écrire à Bertrand Chartier
- [ ] Laclasse.com 
= prévoir un temps de travail pour test du connecteur avec les dev
= prévoir temps tester groupes
- [ ] problème de création des groupes
= travail en cours, OCT/NOV ? 
- [ ] problème de non acceptation des données ENT Kosmos (mais pas Cybercollège ?)
= C. DUCLAUX, négociation avec Kosmos

- [ ] comment aider à la migration de cours sans droits spécifiques ? idem pour chaque établissement
- [ ] fil infos descendant du National (être tenu au courant ...)
- [ ] comment faire remonter une demande de plugins elaastic (recommandation cnesco)
- [ ] aide en ligne ? lenteur + CORRESPONDANTE À vERSAILLES = projet Docusaurus ? on fait nos aides tout seuls, on attend ?
- [ ] création d'une classe + profs pour formation
- [ ] impossible de supprimer un établissement d'un bassin ?
- [ ] impossible d'importer depuis identifiants ENT




##### A faire
- [ ] mode opératoire demande accès Èléa + AJOUTER Raccourci DANS ENT 
(42-01-69, MÉTROPOLE, lYCÉES)
- [ ] mode opératoire Création Classes/groupes ??

##### Communication 
- [ ] préparation webinaires + fixer les dates
- [ ] communications particulières pour les étb 01 et 69 ayant conserver Moodle Kosmos

##### ASSISTANCE : 
- [ ] validation Formulaire + adresse mail de création direct de ticket ?
(Réponse : Suite à votre demande relative à l'assistance ELEA sur les académies de Clermont-Ferrand, Grenoble et Lyon, nous vous proposons de mettre ce sujet à l'ordre du jour du prochain comité SIASI-DRANE.)
- [ ] réduction des catégories de services dans le SUMMIT ?
- comment anticiper le passage d'un parcours type PSC1 de 3.9 à 4.1

----



  <iframe loading="lazy" src="https:&#x2F;&#x2F;www.canva.com&#x2F;design&#x2F;DAFkqXPACoY&#x2F;view?embed" allowfullscreen="allowfullscreen" allow="fullscreen">
  </iframe>



## 1 - Déploiement
ÉTAT : déploiement OK
- Inscription des établissements en cours sur demande : 60 étabs au 14 novembre

**CONNECTEURS**

- MultiCAS pour toutes les plateformes (le sutilisateurs sont crées à la connexion, pas d'importation pour l'instant)
- **Attention envoyer un mail à Rémi avec UAI et nom de l'atb pour les établissements créé dans le 42**
- [ ] faire un point avec B. Chartier pour IMPORT (récupération des groupes/classes, Occitanie ?)

- [ ] Chaque établissement devra créer son propre raccourci/connecteur dans l'ENT = TUTOS à Faire

- [ ] problème du consentement
- [ ] évolution du compte manager
- possibilité d'importer (?)
- possibilité d'aller sur tous les cours (dev Rennes)


## 2 - Migration
C. Duclaux doit voir avec Kosmos pour prolongement gracieux de Moodle Kosmos

**Collectivités**
:::danger Contact 1er semaine de juillet !!!
:::

<Tabs>
  <TabItem value="Ain" label="Ain-01" default>
    Abonnement suspendu sauf pour une short listes d'établissement pour assurer la pérénnité des contenus
0010008D 	Collège Saint-Exupéry (COL - 0010008D)
0011388D 	Collège de Péron (COL - 0011388D
0010035H 	Collège Louis Lumière (COL - 0010035H)
0010975E 	Collège Jacques Prévert (COL - 0010975E)
0010005A 	Collège Roger Poulnard (COL - 0010005A)
0010802S 	Collège Ampère (COL - 0010802S)
0011068F 	Collège Xavier Bichat (COL - 0011068F)
0010794H 	Collège du Valromey (COL - 0010794H)
0011301J 	Collège le Joran (COL - 0011301J)
Collège Dagnieu
  </TabItem>
  <TabItem value="Rhone" label="Rhone-69">
    Abonnement suspendu sauf pour une short listes d'établissement pour assurer la pérénnité des contenus
0690117C 	Collège Jean-Claude Ruet (COL - 0690117C)
0692420F 	Collège Maurice Utrillo (COL - 0692420F)
0691645N 	Collège Faubert (COL - 0691645N)
0692165D 	Cité scolaire Elie Vignal (COL - 0692165D)
0690002C 	Collège Asa Paulini (COL - 0690002C)
0690001B 	Collège Eugénie de Pomey (COL - 0690001B)
0691482L 	Collège Les Quatre Vents (COL - 0691482L)
0693286X 	Collège Jacques Coeur (COL - 0693286X)
0692695E 	Collège Lacassagne (CLG - 0692695E)
0690080M 	Collège Le Bois Franc (COL - 0690080M)
0693975W 	Collège Simone Veil (COL - 0693975W)
0693365H 	Collège Françoise Dolto (COL - 0693365H)
0693890D 	Collège Georges Charpak (COL - 0693890D)
Collège du Petit Pont
  </TabItem>
  <TabItem value="Metropole" label="Metropole Lyon">
    Pas de migrations
  </TabItem>
  <TabItem value="Loire" label="Loire-42">
   Pas de migrations
  </TabItem>
  <TabItem value="Région" label="Région">
   C. Duclaux gère avec Florence Baizeau
  </TabItem>
</Tabs>



## 3 - Accompagnement/Formation

**Equipe de formateur**

- [http://localhost:3000/docusaurus/http://localhost:3000/docusaurus/] Paiements année 2022/2023
- [ ] Prochaine réunion de travail : Vendredi 30 juin
- [ ] GEE : remontée des tests et évolution sur parcours M@g : 

**Webinaires**
Prévision d'un cycle de webinaires dès la rentrée.

- [Webinaire Chef d'établissement](Webinaires/webinaire_ChefEtab)
- [Weinaire Référent Numérique](Webinaires/webinaire_refnum)

**Formations**

## 4 - Assistance
- En attente de validation du formulaire permettant aux utilisateurs de faire remonter un problème sur le SUMMIT (sans aller sur l'interface du SUMMIT)
    - [ ] création formulaire Spip
    - [ ] création et paramétrage suport_elea@ac-lyon.fr ==> DSI

- Pour user : accès au Summit par Portail puis  Support et Assistance > Outil de gestion des incidents et des demandes > Centre de services et d’accompagnement
ou lien direct :
https://sumitlyo.phm.education.gouv.fr/pages/exec.php?exec_module=itop-portal-base&exec_page=index.php&portal_id=itop-portal
puis mot clé elea ou moodle
L'utilisateur doit ensuite choisir entre des services :
![Alt text](../../static/img/image.png)

## 5 - Autres infos en parallèle

- MoodleMoot retransmis par Grand Est : https://www.monbureaunumerique.fr/moodleweek/
- H5p et l'IA : https://h5p.org/june-2023-release-note




<details><summary>Blocs déroulant</summary>
<img src="../../../static/img/docusaurus.png" className='printscreen' style={{float:'right',maxWidth:'40%'}} />
C'est très pratique pour y mettre beacuoup d'infos sans trop encombrer la page
</details>

Tablettes sqool --> Elea
BYOD Veauche --> diffuser usages au quotidien et réemployer les tablettes dans d'autres contextes.
Marc Seguin --> REP+


Convention Cadre CP novembre

Webinaire Sqool demain AM pour le 42



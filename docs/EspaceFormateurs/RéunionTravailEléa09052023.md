# Ordre du jour Réunion 09052023

 **Objectif** : préparation 2023/2024

[TOC]



## 1 - Le point actuel et prévisions 2023/2024

#### a) Dates

Importante !!

#### b) IAM : la communauté Moodle

- groupes dev
- groupe maths
- groupe Cours modèle
- groupe référentiel de compétence

#### c) Les acteurs

- Chefs de projet chargés de la relation au national =  DRANE adjoint Grenoble/Clermont
- GEE
- Equipe locale Moodlers

## 2 - Webinaires

#### **Public** : enseignants

**Objectif** : découvrir ÉLÉA pour inspirer, donner envie de se lancer

**Modalité** : distanciel, classe virtuelle

**Temps** : 45mn

- 10 mn  : vivre un parcours

- 20 mn de présentation

- 15mn de questions (optionnel)

------

#### **Public** : chef d’établissement

**Objectif** : découvrir ÉLÉA pour impulser une dynamique dans l’établissement à l’aide d’un CTA

**Modalité** : distanciel, classe virtuelle

**Temps** : 45mn

- 10 mn  : vivre un parcours

- 20 mn de présentation

- 15mn de questions (optionnel)

------

#### **Public** : référents numériques Éléa

**Objectif** : définir et présenter les missions autour de la plateforme Éléa de son établissement , les accompagnements possibles

**Modalité** : distanciel, classe virtuelle

**Temps** : 45mn

- 10 mn  : vivre un parcours

- 20 mn de présentation

- 15mn de questions (optionnel)

------

#### **Public** : Inspecteurs / IAN

**Objectif** : les missions autour de la plateforme, les formations possibles

**Modalité** : distanciel, classe virtuelle

**Temps** : 45mn

- 10 mn  : vivre un parcours

- 20 mn de présentation

- 15mn de questions (optionnel)

## 3 - Formations

### 3.1 - Parcours Candidature individuelle

### 3.2 - Parcours CTA

# Module 2 - Moodle : Scénariser un contenu de cours

François Lacour
Éric Bugnet
  
**Objectifs**
    • concevoir une scénarisation adaptée à ses besoins avec Moodle
  
 **Contenus**
    • analyse réflexive d’un contenu de cours (type d’apprentissage, modalités, ...)
    • découverte des activités Moodle
    • liaison entre les besoins nés de l’analyse et les possibilités Moodle 
  
**Durée** : 6H
  
**Modalité** : Présentiel
  
 # Déroulé
  
**Prérequis** :
\- connaissance des 6 modes d’apprentissage de la méthode ABC Learning Design
→ test de connaissances pour les participants
\- séléction d’une séance/séquence personnelle
  
**1er temps** : Scénarisation (Hors Moodle)
ABC Learning Design
groupe de 4
  
  
**2ème temps** : A partir des besoins identifiés dans le scénario
→ recherche de l’activité Moodle qui peut répondre au besoin (mode d’apprentissage)
→ découvret et présentation ds activités possibles
  
 ![](https://minio.apps.education.fr/codimd-prod/uploads/upload_d359082c97758c4069e5a4daf3c79488.png)
 

  
Chaque binôme choisit une activité, prend connaissance avec les tutos et la présente aux autres
  
**3ème temps** : choix et association de mode d’apprentissage / activité moodle dans les scénarios
    • articulation présentiel/distanciel, synchrone/asynchrone
    • modification du scénario
    
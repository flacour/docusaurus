![Alt text](../../../static/img/Formations/04_Module.png)![Alt text](../../../static/img/Formations/05_Module.png)

Les participants ont :
- vu les fondamentaux de Moodle
- compris l'importance d'une scénarisation
- survoler l'outil d'aide à la scénarisation ABC LD
- ont en main leur scénario papier décorrélé de Moodle


Objectifs :
Quels gabarits en face de ma scénarisation ?
-  Inverser le temps
- Parcours express
- navigation visible*
- Parcours gamifié

Quelles activités Moodle en face de mes besoisn et typologie ABC LD ?
Intégrer mon parcours
Inscrire des utilisateurs


**MATIN**
Travail sur la scénarisation et la découverte d'activités

**APRÈS MIDI**
Intégration de son parcours

**Ressources**
    
<iframe src="https://h5p.blog.ac-lyon.fr/wordpress/wp-admin/admin-ajax.php?action=h5p_embed&id=5" width="958" height="604" frameborder="0" allowfullscreen="allowfullscreen" title="Présentation ABC LD"></iframe><script src="https://h5p.blog.ac-lyon.fr/wordpress/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>



[Cartes ABCLD](../../../static/img/Documents/ExportIAM_ABC.zip)

MODULE : FORMATION / PRÉSENTIEL / 6H


![mf1](../../../static/img/Formations/01_Module.png)


# 1- Présentation globale Moodle
- présentation, avantages
- contexte et spécificilités locales (assistance, forum perédagogique etc ...)
- mutualisation (?)

<iframe src="https://h5p.blog.ac-lyon.fr/wordpress/wp-admin/admin-ajax.php?action=h5p_embed&id=4" width="958" height="743" frameborder="0" allowfullscreen="allowfullscreen" title="Présentation d&#039;ÉLÉA"></iframe><script src="https://h5p.blog.ac-lyon.fr/wordpress/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

# 2- Qu'est ce qu'un parcours ?

Capsuile du parcours national : https://magistere.education.fr/dgesco/course/view.php?id=2713&section=4

==> inspiration

# 3- Découverte de parcours

- 4 ou 5 parcours type sélectionner dans la Eléathèque à parcourir avec un objectif double (voir ce quipeut se faire et commencer à "analyser")
    - dépot de ressources
    - accompagnement présentiel
    - entrainement autonomie
    - différenciation
    - travail maison
    - ...
- trame d'"analyse" à construire : peut-être un simple question/test 
- permettra de montrer le suivi des élèves grâce aux résultats

CONCLUSION : potentiel et possibilités multiples pour s'adapter à chacun ET nécessité de scénariser

# 4- Importance du scénario pédagogique

https://magistere.education.fr/dgesco/course/view.php?id=2713&section=5

# 5- Prise en main technique : les fondamentaux

Objectif : réaliser un parcours (snas se soucier de scénarisation) avec une liste d'éléments :
- [ ] une ressource vidéo intégrée
- [ ] un contenu de cours type page
- [ ] un devoir



##

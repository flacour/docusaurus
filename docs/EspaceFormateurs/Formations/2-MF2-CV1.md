![Alt text](../../../static/img/Formations/02_Module.png)

![Alt text](../../../static/img/Formations/03_Module.png)

Ces modules sont d'inspiration la partie Scénariser du parcours Action


## Classe virtuelle 
La classe virtuelle ressemblera à celle du parcours national
### Identifier la structure d'un parcours
Pendant la classe virtuelle nous vous avons proposé d'identifier le scénario du parcours "droits d'auteur" que vous avez testé durant votre temps d'inspiration ?

Ce travail a été réalisé à partir de cartes de scénarisation inspirées de la méthode ABC Learning Design

https://iam.unistra.fr/pluginfile.php/5279/mod_resource/content/6/draganddrop.html

### consignes préparatoires pour le module de transférabilité


## Livrable :
Il s'agit de préparer le scénario
[FicheScénario](../../../static/img/Documents/FicheScenarisation.odt)
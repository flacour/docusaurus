
# WEBINAIRE_Chef d'établissemment

**Cible** : Chef d'établissemment

:::tip **Objectifs** : 
- découvrir les potentialités d'Éléa pour mieux l'intégrer dans le pilotage de son établissement
- connaître les possibilités d'accompagnement et de formation d'Éléa
:::

**Durée / Fréquence** : 30mn + 15 mn de questions / 3 (sur 3 semaines)
- novembre/décembre
- Second cycle à prévoir en Janvier.

# Scénario du webinaire

**Prérequis** : Article DRANE à lire + inscription
Teaser à faire ?

## Elea dans l'écosystème de l'établissement
(5mn) Introduction : Projet National (avec réseau des concepteurs ) + Diapo Ecosystème

Différences avec les autres outils de l'ENT : son objectif → outil pédagogique. Mettre ses cours en ligne, enrichir ses cours, collaboration, scénarisation, mise à disposition de ressources, création de contenu pédago, différenciation...

## Qu'est-ce que la plateforme Elea ?
(10 mn) Diapo H5P
<iframe src="https://h5p.blog.ac-lyon.fr/wordpress/wp-admin/admin-ajax.php?action=h5p_embed&id=4" width="958" height="743" frameborder="0" allowfullscreen="allowfullscreen" title="Présentation d&#039;ÉLÉA"></iframe><script src="https://h5p.blog.ac-lyon.fr/wordpress/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

- Activités et ressources (pour toute la communauté éducative, mise a dispo ressources & activités interactives)
- Collaboration / Co-construction (travail en groupe de projet, chef chef-d'œuvre
- Suivi des élèves (tableau de bord, connexions, nb tentatives, dépôt d'un travail, taux réussite...)
- Eleathèque (partage de contenu au niveau national, banque de ressources)
- Pérennité, exportabilité
- Continuité liaison école collège lycée
- Prise en main adaptative selon le niveau maitrise de l'enseignant (débutant / confirmé)
- Accessible par l'ENT (ou directement, mais on le dit pas ?), RGPD, groupes classes gérés par l'ENT

## Démonstration côté élève/prof 
(5 mn)
- Types de cours, des exemples pour un proviseur
- tableau de bord
- création ???

## Initialisation des plateformes
(5 mn)
- article
- calendrier des évolutions/améliorations à venir

## Accompagnement / Formations
(5mn)
- des webinaires pour les chefs d'atb, les IAN/Inspecteurs et les enseignants.
- CTA (25h, présentiel + distanciel, sur 18 mois ?)
- individuelles (idem)
- orienté débutants dans un premier temps
- Webinaires spécifiques ?

## Assistance
(1 mn)
- SUMMIT (francois.lacour@ac-lyon.fr)
- Documentation (docusaurus)
- GEE Groupe pour les évolutions







---



# WEBINAIRE des référents numériques

**Cible** : Référents numériques

:::tip **Objectifs** : 
- découvrir les potentialités pédagogiques d'Éléa afin de pouvoir en faire la promotion dans son établissement
- connaître les possibilités d'accompagnement au développement d'Éléa
- être en capacité de lancer techniquement Éléa dans son établissement
:::

**Durée / Fréquence** : 30mn + 15 mn de questions / 3 (sur 3 semaines)
- du 15 sept au 15 octobre
- Second cycle à prévoir en Janvier.

# Scénario du webinaire

**Prérequis** : Les participants sont invités à avoir un expérience d'un parcours "Droits à l'image" à tester en amont
ATTENTION : pas certitude de pouvoir mettre à dispo un parcours pour des utilisateurs de différentes plateformes

## Elea dans l'écosystème de l'établissement
(5mn) Introduction : Diapo Ecosystème

![Ecosysteme](<../../../static/img/Ecosystème Etablissement.png>)


Différences avec les autres outils de l'ENT : son objectif → outil pédagogique. Mettre ses cours en ligne, enrichir ses cours, collaboration, scénarisation, mise à disposition de ressources, création de contenu pédago, différenciation...

## Qu'est-ce que la plateforme Elea ?
(10 mn) Diapo H5P

<iframe src="https://h5p.blog.ac-lyon.fr/wordpress/wp-admin/admin-ajax.php?action=h5p_embed&id=4" width="958" height="743" frameborder="0" allowfullscreen="allowfullscreen" title="Présentation d&#039;ÉLÉA"></iframe><script src="https://h5p.blog.ac-lyon.fr/wordpress/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>


:::info Note aux formateurs
Bien adapter les points sur lesquels on insiste selon le public
:::

## Démonstration côté élève/prof 
(5 mn)
- 2 types de cours
- tableau de bord
- création ???

## Technique : Initialisation des plateformes
(5 mn)
- besoin importer élèves en début d'année, puis synchro auto ?
- calendrier
- migration (Moodle → Elea / 3.9 → 4.1)

## Accompagnement / Formations
(5mn)
- des webinaires pour les chefs d'atb, les IAN/Inspecteurs et les enseignants.
- CTA (25h, présentiel + distanciel, sur 18 mois ?)
- individuelles (idem)
- orienté débutants dans un premier temps
- Webinaires spécifiques ?

## Assistance
(1 mn)
- SUMMIT (francois.lacour@ac-lyon.fr)
- Documentation (docusaurus)
- GEE Groupe pour les évolutions







---


import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Migration : de Moodle à ÉLÉA

# Les sauvegardes à effectuer en cas de migration d'une plateforme à une autre

:::danger Check-liste
Activités sur lesquelles une action est nécessaire lors de la migration
- [ ] base de données
- [ ] glossaire
- [ ] activités H5P
- [ ] wiki
:::

## Base de données
Dans une activité Base de données d'un cours Moodle :

1- Cliquer sur l'onglet "Actions"
2- Cliquer sur exporter des fiches
![alt text](../../../static/img/Documentation/bdd1.png)
3- Cliquer sur "exporter des fiches"
![alt text](../../../static/img/Documentation/bdd2.jpg)

:::danger Le format "csv" ou "ods" ne permettent pas d'exporter les images ou vidéos insérées dans les articles !
:::

## Glossaire

Dans une activité Glossaire d'un cours Moodle :

1- Cliquer pour dérouler la liste
2- Cliquer sur "Exporter les articles vers un fichier"
![alt text](../../../static/img/Documentation/glossaire1.jpg)
3- Cliquer sur "Exporter"
![alt text](../../../static/img/Documentation/glossaire2.jpg)
3- Cliquer sur "Exporter"
:::danger 
Le format "XML" ne permet pas d'exporter les images ou vidéos insérées dans les articles !
:::

## Une activité H5P

1- dérouler l'onglet "Plus"
2- Cliquer sur l’onglet « Banque de contenus »
![alt text](../../../static/img/Documentation/h5p1.png)
3- cliquer sur l'onglet H5P Cible
![alt text](../../../static/img/Documentation/h5p2.png)
4- Dérouler l’onglet « Plus »
5- Cliquer sur l’onglet « Télécharger »

## Le wiki
:::danger 
Il ne peut pas être sauvegardé !!
:::

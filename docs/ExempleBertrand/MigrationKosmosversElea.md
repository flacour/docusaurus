
# Migrer un parcours de Kosmos à Éléa


Je dois passer par deux étapes :

- [ ] sauvegarder mon cours en local
- [ ] restaurer ce cours sur la plateforme Éléa

## 🔵 Etape 1 : Sauvegarde de mon cours

<iframe src="https://podeduc.apps.education.fr/video/18120-sauvegardedetails_coursmoodlekosmos/193ad23e8ba2208d4347a1ee776f59f270237b251506e845805833e9e3bdc477/?is_iframe=true" width="500" height="360" allowfullscreen ></iframe>

Dans la zone de sauvegarde, vous pouvez télécharger votre fichier de sauvegarde sur votre ordinateur. 

Gardez le précieusement. Il vous servira pour l'étape 2.

*Nota bene* : possibilité de réaliser une sauvegarde supplémentaire de sécurité 

----

Pour disposer d’une sauvegarde supplémentaire, vous pouvez également le déposer sur l’espace académique :

- dans votre cours, utilisez la roue crantée, Publier sur le Campus commun
- prévenez par mail francois.lacour@ac-lyon.fr qui l’archivera dans le dossier adéquat

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_e79d1ed72e8953c954592e530c0365f0.png)


----




## 🔵 Etape 2 : Restaurer son cours sur la plateforme Éléa

<iframe src="https://podeduc.apps.education.fr/video/18152-restaurationcourskosmosverselea/e360b15fa79e492e571ede049eacef26f96c3d03e61fe23d46ca024e8d1f80c0/?is_iframe=true" width="640" height="360" allowfullscreen ></iframe>

Créer un nouveau parcours en utilisant **impérativement** le gabarit "parcours vide" !

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_c6a7475ad9a158d03e901edc0c13d954.png)

Rendez-vous dans ce nouveau parcours

Cliquez sur le bouton bleu en haut à droite pour ouvrir le menu 
![](https://minio.apps.education.fr/codimd-prod/uploads/upload_0962e173bd435a92a7a8b8ddfede4402.png)


Sélectionnez Administration du cours" 
![](https://minio.apps.education.fr/codimd-prod/uploads/upload_7bed12ba233d912713201455723ee5d4.png)


puis restaurer
![](https://minio.apps.education.fr/codimd-prod/uploads/upload_398696b262bd29adc176765faeb94cb5.png)


Importez le fichier de sauvegarde de votre parcours. Vous avez deux possibilités :

- Cliquez sur le bouton "Choisir un fichier" et sélectionnez dans votre ordinateur la sauvegarde que vous venez de faire de votre parcours.
![](https://minio.apps.education.fr/codimd-prod/uploads/upload_27675bab20e888ab2f2d8eeca948b2e3.png)

- Faire un glisser votre fichier de sauvegarde dans la zone à cet effet
![](https://minio.apps.education.fr/codimd-prod/uploads/upload_cf97cc4de5657f0e5131df1e91fb81a1.png)


Il ne vous reste plus qu'à cliquer sur "Restaurer".
![](https://minio.apps.education.fr/codimd-prod/uploads/upload_a6f84d691994a699d4ad2833d1d65beb.png)


Cliquez en bas à droite sur "Continuer".

Cochez **impérativement** "Fusionner le cours sauvegardé avec ce cours". Cliquez sur "Continuer".

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_6b97f4c1aff2dcbdd351129660db6ed8.png)


Lors de cette phase de la restauration, il vous est possible de :
- renommer votre cours, 
- de définir une date de début du cours
- de conserver les groupes

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_c9fdd8aacd99b703a656092003aadfe7.png)


- De restaurer qu'une partie du parcours sauvegardé

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_5ebbce5494e577b7f5d503e6a45d3765.png)


Cliquez sur "Effectuer la restauration" puis sur "Continuer" à 2 reprises.



Attention : si le parcours modèle comprend des badges, il convient de les [ajouter](https://communaute.elea.ac-versailles.fr/local/faq/?role=prof&element=concevoir-des-parcours&item=ajouter-un-badge "tutoriel pour ajouter un badge") manuellement.








import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Déploiement',
    Svg: require('@site/static/img/1.svg').default,
    description: (
      <>
        Stratégie et étapes du déploiement en Académie
      </>
    ),
  },
  {
    title: 'Formation',
    Svg: require('@site/static/img/2.svg').default,
    description: (
      <>
        Dispositifs d'accompagnement et de formation 
      </>
    ),
  },
  {
    title: 'Communication',
    Svg: require('@site/static/img/3.svg').default,
    description: (
      <>
        Publics visés, outils, canaux.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}

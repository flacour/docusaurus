# Website

Pour visualiser le site:

[https://flacour.forge.aeif.fr/docusaurus/](https://flacour.forge.aeif.fr/docusaurus/)

### Fork

Si vous faites un fork, vous devez modifier à minima les lignes 14 et peut-être 17 de docusaurus.config.js

